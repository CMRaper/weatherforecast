package com.christianraper.weatherforecast.models.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.models.entities.Main
import com.christianraper.weatherforecast.models.entities.Weather
import com.christianraper.weatherforecast.models.retrofit.ApiService
import com.christianraper.weatherforecast.models.room.AppDatabase
import com.christianraper.weatherforecast.others.ConstantManager
import com.christianraper.weatherforecast.models.retrofit.requests.FakeApiRequestManager
import com.google.common.truth.Truth.assertThat
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class RepositoryTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var apiRequestManager: FakeApiRequestManager
    private lateinit var repository: Repository

    @Before
    fun setUp(){
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries()
            .build()

        val apiService: ApiService = mockk()

        apiRequestManager = FakeApiRequestManager(apiService)
        repository = Repository(apiRequestManager, database)
    }

    @After
    fun tearDown(){
        database.close()
    }

    @Test
    fun shouldFetchCityWeatherBasedOnCityId() = runBlocking{
        val cityWeather = repository.fetchCityWeather(ConstantManager.MANILA_ID)

        assertThat(cityWeather).isNotNull()
        assertThat(cityWeather.id).isEqualTo(ConstantManager.MANILA_ID)
    }

    @Test
    fun shouldFetchCitiesWeatherBasedOnCityIds() = runBlocking{
        val cityWeathers = repository.fetchWeatherOfCities(
            listOf(ConstantManager.MANILA_ID, ConstantManager.PRAGUE_ID, ConstantManager.SEOUL_ID,))

        assertThat(cityWeathers).isNotNull()
        assertThat(cityWeathers.weathers).isNotEmpty()
    }

    @Test
    fun fetchingCitiesWeatherFromRemote_shouldReturnTheResponse_andSaveTheDataToLocal() = runBlockingTest{
        val citiesWeatherFromRemote = repository.fetchWeatherOfCities(
            listOf(ConstantManager.MANILA_ID, ConstantManager.PRAGUE_ID, ConstantManager.SEOUL_ID))

        val citiesWeatherFromLocal = database.cityWeatherDao().fetchCitiesWeather(
            listOf(ConstantManager.MANILA_ID, ConstantManager.PRAGUE_ID, ConstantManager.SEOUL_ID))

        assertThat(citiesWeatherFromRemote).isNotNull()
        assertThat(citiesWeatherFromRemote.weathers)
            .containsExactlyElementsIn(citiesWeatherFromLocal)
    }

    @Test
    fun fetchingCitiesWeatherFromLocal_shouldReturnDataFromLocal() = runBlockingTest{
        val cityId = 4455
        val mainWeather = Main(
            temperature = 23.5.toFloat(), feelsLike = 29.toFloat(),
            tempMin = 21.2.toFloat(), tempMax = 28.5.toFloat(),
            pressure = 100.toFloat(), humidity = 66.toFloat()
        )
        mainWeather.cityId = cityId
        val weather = Weather(
            id = 1, main = "Rainy",
            description = "Heavy rain", icon = "10d"
        )
        weather.cityId = cityId
        val cityWeather = CityWeather(
            base = "stations", visibility = 10000,
            dt = 1601278940, timezone = 28800,
            id = cityId, name = "Mumbai", cod = 200
        )

        cityWeather.weather = listOf(weather)
        cityWeather.main = mainWeather

        database.cityWeatherDao().insertAll(listOf(cityWeather))
        database.mainDao().insertAll(listOf(mainWeather))
        database.weatherDao().insertAll(listOf(weather))

        val citiesWeather = repository.fetchWeatherOfCities(listOf(cityId), true)

        assertThat(citiesWeather).isNotNull()
        assertThat(citiesWeather.weathers).containsExactlyElementsIn(listOf(cityWeather))
    }

    @Test
    fun fetchingCityWeatherFromLocal_shouldReturnDataFromLocal() = runBlockingTest{
        val cityId = 4455
        val mainWeather = Main(
            temperature = 23.5.toFloat(), feelsLike = 29.toFloat(),
            tempMin = 21.2.toFloat(), tempMax = 28.5.toFloat(),
            pressure = 100.toFloat(), humidity = 66.toFloat()
        )
        mainWeather.cityId = cityId
        val weather = Weather(
            id = 1, main = "Rainy",
            description = "Heavy rain", icon = "10d"
        )
        weather.cityId = cityId
        val cityWeather = CityWeather(
            base = "stations", visibility = 10000,
            dt = 1601278940, timezone = 28800,
            id = cityId, name = "Mumbai", cod = 200
        )

        cityWeather.weather = listOf(weather)
        cityWeather.main = mainWeather

        database.cityWeatherDao().insertAll(listOf(cityWeather))
        database.mainDao().insertAll(listOf(mainWeather))
        database.weatherDao().insertAll(listOf(weather))

        val singelCityWeather = repository.fetchCityWeather(cityId, true)

        assertThat(singelCityWeather).isEqualTo(cityWeather)
    }
}