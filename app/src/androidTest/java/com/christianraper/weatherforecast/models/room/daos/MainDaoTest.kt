package com.christianraper.weatherforecast.models.room.daos

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.christianraper.weatherforecast.models.entities.Main
import com.christianraper.weatherforecast.models.room.AppDatabase
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class MainDaoTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var mainDao: MainDao

    @Before
    fun setUp(){
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        mainDao = database.mainDao()
    }

    @After
    fun tearDown(){
        database.close()
    }

    @Test
    fun shouldSaveAllMainWeather() = runBlockingTest{
        val cityId1 = 112233
        val cityId2 = 223344
        val mainWeather1 = Main(
            temperature = 20.5.toFloat(), feelsLike = 28.toFloat(),
            tempMin = 12.toFloat(), tempMax = 26.toFloat(),
            pressure = 100.toFloat(), humidity = 66.toFloat())
        mainWeather1.cityId = cityId1

        val mainWeather2 = Main(
            temperature = 20.5.toFloat(), feelsLike = 28.toFloat(),
            tempMin = 12.toFloat(), tempMax = 26.toFloat(),
            pressure = 100.toFloat(), humidity = 66.toFloat())
        mainWeather2.cityId = cityId2

        mainDao.insertAll(listOf(mainWeather1, mainWeather2))

        val mains = mainDao.fetchMains(listOf(cityId1, cityId2))
        assertThat(mains).containsExactlyElementsIn(listOf(mainWeather1, mainWeather2))
    }

    @Test
    fun shouldDeleteAllMainWeatherBaseOnCityId() = runBlockingTest{
        val cityId1 = 112233
        val cityId2 = 223344
        val mainWeather1 = Main(
            temperature = 20.5.toFloat(), feelsLike = 28.toFloat(),
            tempMin = 12.toFloat(), tempMax = 26.toFloat(),
            pressure = 100.toFloat(), humidity = 66.toFloat())
        mainWeather1.cityId = cityId1

        val mainWeather2 = Main(
            temperature = 20.5.toFloat(), feelsLike = 28.toFloat(),
            tempMin = 12.toFloat(), tempMax = 26.toFloat(),
            pressure = 100.toFloat(), humidity = 66.toFloat())
        mainWeather2.cityId = cityId2

        mainDao.insertAll(listOf(mainWeather1, mainWeather2))
        mainDao.deleteAll(listOf(cityId1, cityId2))

        val mains = mainDao.fetchMains(listOf(cityId1, cityId2))
        assertThat(mains).isEmpty()
    }
}