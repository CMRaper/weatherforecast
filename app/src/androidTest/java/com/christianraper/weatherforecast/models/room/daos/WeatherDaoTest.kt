package com.christianraper.weatherforecast.models.room.daos

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.christianraper.weatherforecast.models.entities.Weather
import com.christianraper.weatherforecast.models.room.AppDatabase
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class WeatherDaoTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var weatherDao: WeatherDao

    @Before
    fun setUp(){
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        weatherDao = database.weatherDao()
    }

    @After
    fun tearDown(){
        database.close()
    }

    @Test
    fun shouldSaveAllWeather() = runBlockingTest{
        val cityId1 = 1232
        val cityId2 = 2122

        val weather1 = Weather(
            id = 1, main = "Cloudy", description = "Very cloudy", icon = "14D"
        )
        weather1.cityId = cityId1

        val weather2 = Weather(
            id = 2, main = "Sunny", description = "Sunny all day", icon = "10D"
        )
        weather2.cityId = cityId2

        weatherDao.insertAll(listOf(weather1, weather2))
        val weathers = weatherDao.fetchWeathers(listOf(cityId1, cityId2))
        assertThat(weathers).containsExactlyElementsIn(listOf(weather1, weather2))
    }

    @Test
    fun shouldDeleteAllWeatherBaseOnCityId() = runBlockingTest{
        val cityId1 = 1232
        val cityId2 = 2122

        val weather1 = Weather(
            id = 1, main = "Cloudy", description = "Very cloudy", icon = "14D"
        )
        weather1.cityId = cityId1

        val weather2 = Weather(
            id = 2, main = "Sunny", description = "Sunny all day", icon = "10D"
        )
        weather2.cityId = cityId2

        weatherDao.insertAll(listOf(weather1, weather2))
        weatherDao.deleteAll(listOf(cityId1, cityId2))
        val weathers = weatherDao.fetchWeathers(listOf(cityId1, cityId2))
        assertThat(weathers).isEmpty()
    }
}