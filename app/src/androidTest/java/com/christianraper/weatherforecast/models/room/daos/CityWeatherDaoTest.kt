package com.christianraper.weatherforecast.models.room.daos

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.models.entities.CityWeatherAndMain
import com.christianraper.weatherforecast.models.entities.Main
import com.christianraper.weatherforecast.models.entities.Weather
import com.christianraper.weatherforecast.models.room.AppDatabase
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class CityWeatherDaoTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var cityWeatherDao: CityWeatherDao

    @Before
    fun setUp(){
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        cityWeatherDao = database.cityWeatherDao()
    }

    @After
    fun tearDown(){
        database.close()
    }

    @Test
    fun shouldSaveAllCityWeather() = runBlockingTest{
        val cityId1 = 112233
        val cityId2 = 334455
        val cityWeather1 = CityWeather(
            base = "stations", visibility = 10000, dt = 1601278940,
            timezone = 28800, id = cityId1, name = "Manila", cod = 200)
        val cityWeather2 = CityWeather(
            base = "stations", visibility = 10000, dt = 1601278940,
            timezone = 28800, id = cityId2, name = "Prague", cod = 200)
        cityWeatherDao.insertAll(listOf(cityWeather1, cityWeather2))

        val cityWeathers = cityWeatherDao.fetchCitiesWeather(listOf(cityId1, cityId2))
        assertThat(cityWeathers).containsExactlyElementsIn(listOf(cityWeather1, cityWeather2))
    }

    @Test
    fun shouldDeleteAllCityWeatherBaseOnCityId() = runBlockingTest{
        val cityId1 = 112233
        val cityId2 = 334455
        val cityWeather1 = CityWeather(
            base = "stations", visibility = 10000, dt = 1601278940,
            timezone = 28800, id = cityId1, name = "Manila", cod = 200)
        val cityWeather2 = CityWeather(
            base = "stations", visibility = 10000, dt = 1601278940,
            timezone = 28800, id = cityId2, name = "Prague", cod = 200)
        cityWeatherDao.insertAll(listOf(cityWeather1, cityWeather2))
        cityWeatherDao.deleteAll(listOf(cityId1, cityId2))

        val cityWeathers = cityWeatherDao.fetchCitiesWeather(listOf(cityId1, cityId2))
        assertThat(cityWeathers).isEmpty()
    }

    @Test
    fun shouldFetchAllCityWeatherBasedOnCityId() = runBlockingTest{
        val cityId = 4455
        val mainWeather = Main(
            temperature = 23.5.toFloat(), feelsLike = 29.toFloat(),
            tempMin = 21.2.toFloat(), tempMax = 28.5.toFloat(),
            pressure = 100.toFloat(), humidity = 66.toFloat()
        )
        mainWeather.cityId = cityId
        val weather = Weather(
            id = 1, main = "Rainy",
            description = "Heavy rain", icon = "10d"
        )
        weather.cityId = cityId
        val cityWeather = CityWeather(
            base = "stations", visibility = 10000,
            dt = 1601278940, timezone = 28800,
            id = cityId, name = "Mumbai", cod = 200
        )

        cityWeather.weather = listOf(weather)
        cityWeather.main = mainWeather

        val cityWeatherWithMain = CityWeatherAndMain(
            cityWeather = cityWeather, main = mainWeather, weather = weather)

        cityWeatherDao.insertAll(listOf(cityWeather))
        database.mainDao().insertAll(listOf(mainWeather))
        database.weatherDao().insertAll(listOf(weather))

        val output = cityWeatherDao.fetchCitiesWeatherAndMain(listOf(cityId))
        assertThat(output).containsExactlyElementsIn(listOf(cityWeatherWithMain))
    }

}