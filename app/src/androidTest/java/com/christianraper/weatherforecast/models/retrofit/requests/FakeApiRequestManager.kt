package com.christianraper.weatherforecast.models.retrofit.requests

import com.christianraper.weatherforecast.models.entities.CitiesWeather
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.models.entities.Main
import com.christianraper.weatherforecast.models.entities.Weather
import com.christianraper.weatherforecast.models.retrofit.ApiService
import com.christianraper.weatherforecast.others.ConstantManager
import javax.inject.Inject

class FakeApiRequestManager @Inject constructor(private val apiService: ApiService):
    ApiRequestManager(apiService) {

    private var citiesWeather: CitiesWeather

    init {
        citiesWeather = generateFakeCitiesWeather()
    }

    override suspend fun fetchCityWeather(cityId: Int): CityWeather {
        return citiesWeather.weathers.find {
            it.id == cityId
        }?: generateFakeCityWeather(cityId, "Manila",
            29.4.toFloat(), 27.3.toFloat(), 31.9.toFloat(),
            "Sunny", "Sunny all day")
    }

    override suspend fun fetchWeatherOfCities(cityIds: String): CitiesWeather {
        return citiesWeather
    }

    private fun generateFakeCitiesWeather() : CitiesWeather{
        val manila = generateFakeCityWeather(ConstantManager.MANILA_ID, "Manila",
            29.4.toFloat(), 27.3.toFloat(), 31.9.toFloat(), "Sunny", "Sunny all day")
        val prague = generateFakeCityWeather(ConstantManager.PRAGUE_ID, "Prague",
            17.2.toFloat(), 18.0.toFloat(), 19.5.toFloat(), "Cloudy", "Partial cloud")
        val seoul = generateFakeCityWeather(ConstantManager.SEOUL_ID, "Seoul",
            5.toFloat(), 3.toFloat(), 8.toFloat(), "Rainy", "Heavy rain")

        val cW = CitiesWeather(
            count = 3, listOf(manila, prague, seoul))
        return cW
    }

    private fun generateFakeCityWeather(cityId: Int, cityName: String,
                                        temp: Float, tempMin: Float, tempMax: Float, main: String,
                                        description: String): CityWeather{
        val mainWeather = Main(
            temperature = temp,
            feelsLike = temp + 10,
            tempMin = tempMin,
            tempMax = tempMax,
            pressure = 100.toFloat(),
            humidity = 66.toFloat()
        )

        val weather = Weather(
            id = 1,
            main = main,
            description = description,
            icon = "10d"
        )

        val cityWeather = CityWeather(
            base = "stations",
            visibility = 10000,
            dt = 1601278940,
            timezone = 28800,
            id = cityId,
            name = cityName,
            cod = 200
        )

        cityWeather.weather = listOf(weather)
        cityWeather.main = mainWeather

        return cityWeather
    }
}