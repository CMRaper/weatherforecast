package com.christianraper.weatherforecast.utils

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.christianraper.weatherforecast.R
import org.junit.Test
import org.junit.runner.RunWith
import com.google.common.truth.Truth.assertThat
import org.junit.Before

@RunWith(AndroidJUnit4::class)
class UtilitiesTest{

    private lateinit var context: Context

    @Before
    fun setUp(){
        context = ApplicationProvider.getApplicationContext()
    }

    @Test
    fun allCorrectParameterValues_shouldReturnProperlyFormattedTemperatureString(){
        val placeholder = context.getString(R.string.temperature_placeholder)
        val expected = "34.6°C"
        val output = Utilities.formatTemperature(placeholder, 34.5656.toFloat(), 1)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun nullTemperature_shouldReturnNAString(){
        val placeholder = context.getString(R.string.temperature_placeholder)
        val expected = "N/A"
        val output = Utilities.formatTemperature(placeholder, null, 1)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun zeroDecimalPlace_shouldReturnProperlyFormattedTemperatureString(){
        val placeholder = context.getString(R.string.temperature_placeholder)
        val expected = "35°C"
        val output = Utilities.formatTemperature(placeholder, 34.5656.toFloat(), 0)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun invalidDecimalPlace_shouldUseZeroAndReturnProperlyFormattedTemperatureString(){
        val placeholder = context.getString(R.string.temperature_placeholder)
        val expected = "35°C"
        val output = Utilities.formatTemperature(placeholder, 34.5656.toFloat(), -10)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun noDecimalPoint_shouldReturnProperlyFormattedTemperatureString(){
        val placeholder = context.getString(R.string.temperature_placeholder)
        val expected = "34.0°C"
        val output = Utilities.formatTemperature(placeholder, 34.toFloat(), 1)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun allCorrectParameterValues_shouldReturnProperlyFormattedHighLowTempString(){
        val placeholder = context.getString(R.string.high_low_placeholder)
        val expected = "High 28.5°C / Low 22.2°C"
        val output = Utilities.formatHighLowTemperature(
            placeholder, 28.512.toFloat(), 22.21.toFloat(), 1)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun nullHighTemperature_shouldReturnNAString(){
        val placeholder = context.getString(R.string.high_low_placeholder)
        val expected = "N/A"
        val output = Utilities.formatHighLowTemperature(
            placeholder,null, 22.21.toFloat(), 1)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun nullLowTemperature_shouldReturnNAString(){
        val placeholder = context.getString(R.string.high_low_placeholder)
        val expected = "N/A"
        val output = Utilities.formatHighLowTemperature(
            placeholder, 28.512.toFloat(), null, 1)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun zeroDecimalPlaceForHighLow_shouldReturnProperlyFormattedString(){
        val placeholder = context.getString(R.string.high_low_placeholder)
        val expected = "High 29°C / Low 22°C"
        val output = Utilities.formatHighLowTemperature(
            placeholder, 28.512.toFloat(), 22.21.toFloat(), 0)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun invalidDecimalPlaceForHighLow_shouldUseZeroAndReturnProperlyFormattedString(){
        val placeholder = context.getString(R.string.high_low_placeholder)
        val expected = "High 30°C / Low 24°C"
        val output = Utilities.formatHighLowTemperature(
            placeholder, 29.876.toFloat(), 23.55.toFloat(), -2)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun noDecimalPointForHighLow_shouldReturnProperlyFormattedString(){
        val placeholder = context.getString(R.string.high_low_placeholder)
        val expected = "High 29.00°C / Low 23.00°C"
        val output = Utilities.formatHighLowTemperature(
            placeholder, 29.toFloat(), 23.toFloat(), 2)
        assertThat(output).isEqualTo(expected)
    }

    @Test
    fun allCorrectParameterValues_shouldReturnTrueIfTheyAreSame(){
        val output = Utilities.isFavorite(1111, 1111)
        assertThat(output).isTrue()
    }

    @Test
    fun allCorrectParameterValues_shouldReturnFalseIfTheyAreNotSame(){
        val output = Utilities.isFavorite(1111, 1122)
        assertThat(output).isFalse()
    }

    @Test
    fun negativeOneSavedCityId_shouldReturnFalse(){
        val output = Utilities.isFavorite(-1, -1)
        assertThat(output).isFalse()
    }

    @Test
    fun nullCityId_shouldReturnFalse(){
        val output = Utilities.isFavorite(1122, null)
        assertThat(output).isFalse()
    }
}