package com.christianraper.weatherforecast.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.christianraper.weatherforecast.R
import com.christianraper.weatherforecast.databinding.CardListWeatherBinding
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.utils.PreferencesManager
import com.christianraper.weatherforecast.utils.Utilities
import javax.inject.Inject

class WeatherAdapter @Inject constructor() : RecyclerView.Adapter<WeatherAdapter.ViewHolder>(){
    private var weathers: MutableList<CityWeather> = mutableListOf()
    private var citiesWeatherListener: CitiesWeatherListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val binding: CardListWeatherBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.card_list_weather, parent, false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: CityWeather = weathers[position]
        holder.bind(item, citiesWeatherListener)
    }

    override fun getItemCount(): Int {
        return weathers.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun setCityWeathers(stops: List<CityWeather>){
        this.weathers.clear()
        this.weathers.addAll(stops)
    }

    fun setCityWeatherListener(citiesWeatherListener: CitiesWeatherListener){
        this.citiesWeatherListener = citiesWeatherListener
    }

    class ViewHolder(
        private val binding: CardListWeatherBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(cityWeather: CityWeather, citiesWeatherListener: CitiesWeatherListener?) {
            binding.cityTextview.text = cityWeather.name
            binding.temperatureTextview.text = Utilities.formatTemperature(
                binding.temperatureTextview.context.getString(R.string.temperature_placeholder),
                cityWeather.main?.temperature, 1)
            val description = if(cityWeather.weather.isNotEmpty()) cityWeather.weather[0].main else ""
            binding.statusTextview.text = description
            binding.favoriteVisible = Utilities.isFavorite(
                PreferencesManager.getFavoriteId(binding.favoriteImageview.context), cityWeather.id)
            binding.temperature = cityWeather.main?.temperature

            binding.root.setOnClickListener {
                citiesWeatherListener?.onItemCityWeatherTapped(cityWeather)
            }
        }
    }

    interface CitiesWeatherListener{
        fun onItemCityWeatherTapped(cityWeather: CityWeather)
    }
}