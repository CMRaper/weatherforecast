package com.christianraper.weatherforecast.adapters

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BindingAdapter<T : ViewDataBinding?> :
    RecyclerView.Adapter<BindingViewHolder<T>>() {
    var position = 0
    override fun onBindViewHolder(holder: BindingViewHolder<T>, position: Int) {
        val binding: T = holder.binding
        updateBinding(binding, position)
    }

    protected abstract fun updateBinding(binding: T, position: Int)
}
