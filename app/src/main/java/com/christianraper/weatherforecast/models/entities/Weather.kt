package com.christianraper.weatherforecast.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.christianraper.weatherforecast.others.ConstantManager

@Entity(tableName = ConstantManager.WEATHER_TABLE)
data class Weather (
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
){
    @ColumnInfo(name="room_id")
    @PrimaryKey(autoGenerate = true)
    var roomId: Int? = null
    @ColumnInfo(name="city_id")
    var cityId: Int? = null
}