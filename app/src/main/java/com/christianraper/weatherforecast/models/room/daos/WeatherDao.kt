package com.christianraper.weatherforecast.models.room.daos

import androidx.room.*
import com.christianraper.weatherforecast.models.entities.Main
import com.christianraper.weatherforecast.models.entities.Weather
import com.christianraper.weatherforecast.others.ConstantManager

@Dao
interface WeatherDao {
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(weathers: List<Weather>)

    @Query("DELETE FROM ${ConstantManager.WEATHER_TABLE} WHERE city_id IN(:cityIds)")
    suspend fun deleteAll(cityIds: List<Int>)

    @Query("SELECT * FROM ${ConstantManager.WEATHER_TABLE} WHERE city_id IN (:cityIds)")
    suspend fun fetchWeathers(cityIds: List<Int>): List<Weather>
}