package com.christianraper.weatherforecast.models.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.models.entities.Main
import com.christianraper.weatherforecast.models.entities.Weather
import com.christianraper.weatherforecast.models.room.daos.CityWeatherDao
import com.christianraper.weatherforecast.models.room.daos.MainDao
import com.christianraper.weatherforecast.models.room.daos.WeatherDao
import com.christianraper.weatherforecast.others.ConstantManager

@Database(
    entities = [CityWeather::class, Main::class, Weather::class],
    version = 1,
    exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cityWeatherDao(): CityWeatherDao
    abstract fun mainDao(): MainDao
    abstract fun weatherDao(): WeatherDao

    companion object {
        // For Singleton instantiation
        @Volatile private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context,
                AppDatabase::class.java, ConstantManager.DATABASE_NAME)
                .build()
        }
    }
}