package com.christianraper.weatherforecast.models.entities

import androidx.room.Embedded
import androidx.room.Relation

data class CityWeatherAndMain(
    @Embedded val cityWeather: CityWeather,
    @Relation(
        parentColumn = "id",
        entityColumn = "city_id")
    val weather: Weather,
    @Relation(
        parentColumn = "id",
        entityColumn = "city_id")
    val main: Main
)