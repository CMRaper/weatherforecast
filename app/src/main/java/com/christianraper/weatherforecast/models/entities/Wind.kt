package com.christianraper.weatherforecast.models.entities

data class Wind (
    val speed: Float,
    val deg: Float)