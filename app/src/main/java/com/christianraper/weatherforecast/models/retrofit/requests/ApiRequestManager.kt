package com.christianraper.weatherforecast.models.retrofit.requests

import com.christianraper.weatherforecast.models.entities.CitiesWeather
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.models.retrofit.ApiService
import javax.inject.Inject

open class ApiRequestManager @Inject constructor(private val apiService: ApiService)  {
    open suspend fun fetchCityWeather(cityId: Int): CityWeather{
        return apiService.fetchCityWeather(cityId)
    }

    open suspend fun fetchWeatherOfCities(cityIds: String): CitiesWeather{
        return apiService.fetchWeatherOfCities(cityIds)
    }
}