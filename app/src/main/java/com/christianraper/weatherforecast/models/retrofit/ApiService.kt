package com.christianraper.weatherforecast.models.retrofit

import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.models.entities.CitiesWeather
import com.christianraper.weatherforecast.others.ConstantManager
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("data/2.5/weather?appid=" + ConstantManager.WEATHER_API_KEY +"&units=metric")
    suspend fun fetchCityWeather(@Query("id") cityId: Int): CityWeather

    @GET("data/2.5/group?appid=" + ConstantManager.WEATHER_API_KEY +"&units=metric")
    suspend fun fetchWeatherOfCities(@Query("id") cityIds: String): CitiesWeather
}