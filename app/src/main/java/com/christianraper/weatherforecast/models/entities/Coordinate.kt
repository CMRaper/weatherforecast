package com.christianraper.weatherforecast.models.entities

data class Coordinate (
    val lon: Double,
    val lat: Double)