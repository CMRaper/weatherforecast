package com.christianraper.weatherforecast.models.room.daos

import androidx.room.*
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.models.entities.CityWeatherAndMain
import com.christianraper.weatherforecast.models.entities.Weather
import com.christianraper.weatherforecast.others.ConstantManager

@Dao
interface CityWeatherDao {
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(citiesWeather: List<CityWeather>)

    @Query("DELETE FROM ${ConstantManager.CITY_WEATHER_TABLE} WHERE id IN(:cityIds)")
    suspend fun deleteAll(cityIds: List<Int>)

    @Query("SELECT * FROM ${ConstantManager.CITY_WEATHER_TABLE} WHERE id IN (:cityIds)")
    suspend fun fetchCitiesWeather(cityIds: List<Int>): List<CityWeather>

    @Transaction
    @Query("SELECT * FROM ${ConstantManager.CITY_WEATHER_TABLE} WHERE id IN (:cityIds)")
    suspend fun fetchCitiesWeatherAndMain(cityIds: List<Int>): List<CityWeatherAndMain>

    @Transaction
    @Query("SELECT * FROM ${ConstantManager.CITY_WEATHER_TABLE} WHERE id = :cityId")
    suspend fun fetchCityWeather(cityId: Int): CityWeatherAndMain
}