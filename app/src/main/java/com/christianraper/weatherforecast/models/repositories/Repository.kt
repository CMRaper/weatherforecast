package com.christianraper.weatherforecast.models.repositories

import com.christianraper.weatherforecast.models.entities.CitiesWeather
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.models.entities.Main
import com.christianraper.weatherforecast.models.entities.Weather
import com.christianraper.weatherforecast.models.retrofit.requests.ApiRequestManager
import com.christianraper.weatherforecast.models.room.AppDatabase
import javax.inject.Inject

class Repository @Inject constructor(
    private val apiRequestManager: ApiRequestManager,
    private val database: AppDatabase) {
    suspend fun fetchCityWeather(cityId: Int, fetchFromLocal: Boolean = false): CityWeather {
        if(fetchFromLocal){
            return fetchCityWeatherFromLocal(cityId)
        }

        return apiRequestManager.fetchCityWeather(cityId)
    }

    suspend fun fetchWeatherOfCities(cityIds: List<Int>, fetchFromLocal: Boolean = false): CitiesWeather {
        if(fetchFromLocal){
            return fetchWeatherOfCities(cityIds)
        }

        val stringCityIds = cityIds.joinToString(separator = ",") { it.toString() }
        val citiesWeather = apiRequestManager.fetchWeatherOfCities(stringCityIds)

        deleteWeatherOfCitiesFromLocal(citiesWeather) //delete first the local data
        saveWeatherOfCitiesToLocal(citiesWeather) //save the newly fetched data to local

        return citiesWeather
    }

    private suspend fun fetchWeatherOfCities(cityIds: List<Int>): CitiesWeather{
        val citiesWeatherFromLocal = database.cityWeatherDao().fetchCitiesWeatherAndMain(cityIds)
        val cityWeatherList: MutableList<CityWeather> = mutableListOf()

        for(c in citiesWeatherFromLocal){
            val cityWeather = CityWeather(
                base = c.cityWeather.base, visibility = c.cityWeather.visibility,
                dt = c.cityWeather.dt, timezone = c.cityWeather.timezone,
                id = c.cityWeather.id, name = c.cityWeather.name,
                cod = c.cityWeather.cod)
            cityWeather.weather = listOf(c.weather)
            cityWeather.main = c.main
            cityWeatherList.add(cityWeather)
        }

        return CitiesWeather(cityWeatherList.size, cityWeatherList)
    }

    private suspend fun saveWeatherOfCitiesToLocal(citiesWeather: CitiesWeather){
        val weathers = citiesWeather.weathers
        val cityWeatherToLocal = mutableListOf<CityWeather>()
        val weatherMainToLocal = mutableListOf<Main>()
        val weatherToLocal = mutableListOf<Weather>()
        for(c in weathers){
            val cityWeather = CityWeather(
                base = c.base, visibility = c.visibility, dt = c.dt,
                timezone = c.timezone, id = c.id, name = c.name, cod = c.cod)

            cityWeatherToLocal.add(cityWeather)

            if(c.weather.isNotEmpty()){
                val w = c.weather[0]
                val weather = Weather(
                    id = w.id, main = w.main,
                    description = w.description, icon = w.icon)
                weather.cityId = c.id
                weatherToLocal.add(weather)
            }

            if(c.main != null){
                val main = Main(
                    temperature = c.main!!.temperature, feelsLike= c.main!!.feelsLike,
                    tempMin= c.main!!.tempMin, tempMax= c.main!!.tempMax,
                    pressure= c.main!!.pressure, humidity= c.main!!.humidity)
                main.cityId = c.id
                weatherMainToLocal.add(main)
            }
        }

        database.cityWeatherDao().insertAll(cityWeatherToLocal)
        database.weatherDao().insertAll(weatherToLocal)
        database.mainDao().insertAll(weatherMainToLocal)
    }

    private suspend fun deleteWeatherOfCitiesFromLocal(citiesWeather: CitiesWeather){
        val weathers = citiesWeather.weathers
        val cityIds = mutableListOf<Int>()
        for(c in weathers){
            cityIds.add(c.id)
        }

        database.cityWeatherDao().deleteAll(cityIds)
        database.weatherDao().deleteAll(cityIds)
        database.mainDao().deleteAll(cityIds)
    }

    private suspend fun fetchCityWeatherFromLocal(cityId: Int): CityWeather{
        val fromLocal = database.cityWeatherDao().fetchCityWeather(cityId)
        val cityWeather = CityWeather(
            base = fromLocal.cityWeather.base, visibility = fromLocal.cityWeather.visibility,
            dt = fromLocal.cityWeather.dt, timezone = fromLocal.cityWeather.timezone,
            id = fromLocal.cityWeather.id, name = fromLocal.cityWeather.name,
            cod = fromLocal.cityWeather.cod)
        cityWeather.weather = listOf(fromLocal.weather)
        cityWeather.main = fromLocal.main

        return cityWeather
    }
}