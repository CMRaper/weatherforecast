package com.christianraper.weatherforecast.models.entities

import com.christianraper.weatherforecast.models.entities.CityWeather
import com.google.gson.annotations.SerializedName

data class CitiesWeather(
    @SerializedName("cnt")
    val count: Int,
    @SerializedName("list")
    val weathers: List<CityWeather>
)