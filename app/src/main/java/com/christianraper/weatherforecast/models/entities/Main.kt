package com.christianraper.weatherforecast.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.christianraper.weatherforecast.others.ConstantManager
import com.google.gson.annotations.SerializedName

@Entity(tableName = ConstantManager.MAIN_TABLE)
data class Main(
    @SerializedName("temp")
    val temperature: Float,
    @SerializedName("feels_like")
    @ColumnInfo(name="feels_like")
    val feelsLike: Float,
    @SerializedName("temp_min")
    @ColumnInfo(name="temp_min")
    val tempMin: Float,
    @SerializedName("temp_max")
    @ColumnInfo(name="tempMax")
    val tempMax: Float,
    val pressure: Float,
    val humidity: Float){

    @ColumnInfo(name="room_id")
    @PrimaryKey(autoGenerate = true)
    var roomId: Int? = null
    @ColumnInfo(name="city_id")
    var cityId: Int? = null
}