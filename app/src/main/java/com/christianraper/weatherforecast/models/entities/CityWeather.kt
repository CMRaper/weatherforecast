package com.christianraper.weatherforecast.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.christianraper.weatherforecast.others.ConstantManager
import com.google.gson.annotations.SerializedName

@Entity(tableName = ConstantManager.CITY_WEATHER_TABLE)
data class CityWeather(
    val base: String?,
    val visibility: Long,
    val dt: Long,
    val timezone: Long?,
    val id: Int,
    val name: String,
    val cod: Int?
){
    @ColumnInfo(name="room_id")
    @PrimaryKey(autoGenerate = true)
    var roomId: Int? = null

    @Ignore
    var weather: List<Weather> = mutableListOf()
    @Ignore
    var main: Main? = null
    @Ignore
    val sys: Country? = null
    @Ignore
    @SerializedName("coord")
    val coordinate: Coordinate? = null
    @Ignore
    val wind: Wind? = null
    @Ignore
    val clouds: Cloud? = null
    @Ignore
    var isFavorite: Boolean = false
}