package com.christianraper.weatherforecast.models.entities

data class Country(
    val type: Int?,
    val id: Int?,
    val timezone: Long,
    val country: String,
    val sunrise: Long,
    val sunset: Long)