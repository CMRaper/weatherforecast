package com.christianraper.weatherforecast.models.room.daos

import androidx.room.*
import com.christianraper.weatherforecast.models.entities.CityWeatherAndMain
import com.christianraper.weatherforecast.models.entities.Main
import com.christianraper.weatherforecast.models.entities.Weather
import com.christianraper.weatherforecast.others.ConstantManager

@Dao
interface MainDao {
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(mains: List<Main>)

    @Query("DELETE FROM ${ConstantManager.MAIN_TABLE} WHERE city_id IN(:cityIds)")
    suspend fun deleteAll(cityIds: List<Int>)

    @Query("SELECT * FROM ${ConstantManager.MAIN_TABLE} WHERE city_id IN (:cityIds)")
    suspend fun fetchMains(cityIds: List<Int>): List<Main>
}