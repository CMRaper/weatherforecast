package com.christianraper.weatherforecast.viewmodels

import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.christianraper.weatherforecast.models.entities.CitiesWeather
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.models.repositories.Repository
import com.christianraper.weatherforecast.others.Resource
import com.christianraper.weatherforecast.others.SingleLiveEvent
import com.christianraper.weatherforecast.others.UIEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {
    var isNoDataVisible: ObservableBoolean = ObservableBoolean(false)
    var isCircularProgressVisible: ObservableBoolean = ObservableBoolean(true)
    var isFavoriteVisible: ObservableBoolean = ObservableBoolean(false)
    var templateText: ObservableField<String> = ObservableField<String>("")

    var showFilledIcon: ObservableBoolean = ObservableBoolean(false)
    val weatherResource: MutableLiveData<Resource<CityWeather>> = MutableLiveData()

    val uiEvent: SingleLiveEvent<UIEvent> = SingleLiveEvent()

    // Events
    fun onFavoriteIconTapped(): View.OnClickListener{
        return View.OnClickListener {
            uiEvent.postValue(UIEvent.FavoriteIconTapped(!showFilledIcon.get()))
            showFilledIcon.set(!showFilledIcon.get())
        }
    }

    // Repository calls
    fun fetchCityWeather(cityId: Int, fetchFromLocal: Boolean){
        viewModelScope.launch(Dispatchers.IO) {
            weatherResource.postValue(Resource.loading(data = null))
            try {
                val response = repository.fetchCityWeather(cityId, fetchFromLocal)
                weatherResource.postValue(Resource.success(data = response))
            } catch (exception: Exception) {
                weatherResource.postValue(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
            }
        }
    }
}