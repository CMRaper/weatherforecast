package com.christianraper.weatherforecast.viewmodels

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.christianraper.weatherforecast.models.entities.CitiesWeather
import com.christianraper.weatherforecast.models.repositories.Repository
import com.christianraper.weatherforecast.others.ConstantManager
import com.christianraper.weatherforecast.others.Resource
import com.christianraper.weatherforecast.utils.PreferencesManager
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class ListViewModel @ViewModelInject constructor(
    private val repository: Repository,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {
    var isRefreshing: ObservableBoolean = ObservableBoolean(false)
    var isNoDataVisible: ObservableBoolean = ObservableBoolean(false)
    var templateText: ObservableField<String> = ObservableField<String>("")
    val weatherResource: MutableLiveData<Resource<CitiesWeather>> = MutableLiveData()

    // Events
    fun onWeathersRefreshed() : SwipeRefreshLayout.OnRefreshListener{
        return SwipeRefreshLayout.OnRefreshListener {
            isRefreshing.set(true)
            fetchWeatherOfCities(getCitiesToFetch())
        }
    }

    // Repository calls
    fun fetchWeatherOfCities(cityIds: List<Int>, fetchFromLocal: Boolean = false){
        viewModelScope.launch(IO) {
            weatherResource.postValue(Resource.loading(data = null))
            try {
                val response = repository.fetchWeatherOfCities(cityIds, fetchFromLocal)
                weatherResource.postValue(Resource.success(data = response))
            } catch (exception: Exception) {
                weatherResource.postValue(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
            }

            isRefreshing.set(false)
        }
    }

    // Others
    fun getCitiesToFetch(): List<Int>{
        val cityIds: MutableList<Int> = mutableListOf()
        cityIds.add(ConstantManager.MANILA_ID)
        cityIds.add(ConstantManager.PRAGUE_ID)
        cityIds.add(ConstantManager.SEOUL_ID)

        return cityIds
    }
}