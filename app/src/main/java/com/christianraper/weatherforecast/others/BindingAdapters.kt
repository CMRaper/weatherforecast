package com.christianraper.weatherforecast.others

import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.christianraper.weatherforecast.R

@BindingAdapter("customBackgroundColor")
fun setCustomBackgroundColor(view: CardView, temp: Float?) {
    val color = if(temp == null){
        R.color.colorWhite
    }else if(temp < 0){
        R.color.colorFreezing
    }else if(temp > 0 && temp < 16){
        R.color.colorCold
    }else if(temp > 16 && temp < 31){
        R.color.colorWarm
    }else{
        R.color.colorHot
    }

    view.setCardBackgroundColor(ContextCompat.getColor(view.context, color))
}