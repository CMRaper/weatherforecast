package com.christianraper.weatherforecast.others

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    SWIPE_REFRESH
}