package com.christianraper.weatherforecast.others

import com.christianraper.weatherforecast.BuildConfig

object ConstantManager {
    const val API_URL = "https://api.openweathermap.org/"
    const val WEATHER_API_KEY = "ad5aa212cd157284c176b875117abc5a"

    const val MANILA_ID = 1701668
    const val PRAGUE_ID = 3067696
    const val SEOUL_ID = 1835848

    const val ERROR_MESSAGE_DURATION = 5000
    const val SUCCESS_MESSAGE_DURATION = 5000

    const val CITIES_WEATHER_LOADED = BuildConfig.APPLICATION_ID + "CITIES_WEATHER_LOADED"
    const val CITY_WEATHER_LOADED = BuildConfig.APPLICATION_ID + "CITY_WEATHER_LOADED"
    const val LIST_FRAGMENT_ATTACHED = BuildConfig.APPLICATION_ID + "LIST_FRAGMENT_ATTACHED"
    const val CITY_FRAGMENT_ATTACHED = BuildConfig.APPLICATION_ID + "CITY_FRAGMENT_ATTACHED"
    const val SELECTED_CITY_ID = BuildConfig.APPLICATION_ID + "SELECTED_CITY_ID"

    const val PREFERENCES_NAME = BuildConfig.APPLICATION_ID + "cityPreferences"

    const val WEATHER_TABLE = "weathers"
    const val MAIN_TABLE = "mains"
    const val CITY_WEATHER_TABLE = "city_weathers"
    const val DATABASE_NAME = "db_weather_forecast"
}