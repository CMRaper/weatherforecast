package com.christianraper.weatherforecast.others

sealed class UIEvent {
    class FavoriteIconTapped(val isFavorite: Boolean) : UIEvent()
}