package com.christianraper.weatherforecast.utils

object Utilities {
    fun formatTemperature(placeholder: String, temp: Float?, decimalPlace: Int): String{
        if(temp == null) return "N/A"
        val safeDecimalPlace = if(decimalPlace < 0) 0 else decimalPlace
        return String.format(placeholder, "%.${safeDecimalPlace}f".format(temp));
    }

    fun formatHighLowTemperature(placeholder: String, high: Float?,
                                 low: Float?, decimalPlace: Int = 0): String{
        if(high == null || low == null) return "N/A"
        val safeDecimalPlace = if(decimalPlace < 0) 0 else decimalPlace
        return String.format(placeholder,
            "%.${safeDecimalPlace}f".format(high), "%.${safeDecimalPlace}f".format(low));
    }

    fun isFavorite(savedCityId: Int, cityId: Int?): Boolean{
        return savedCityId != -1 && cityId != null && savedCityId == cityId
    }
}