package com.christianraper.weatherforecast.utils

import android.content.Context
import android.content.SharedPreferences
import com.christianraper.weatherforecast.others.ConstantManager.PREFERENCES_NAME

object PreferencesManager {
    private const val FAVORITE_ID = "favoriteId"

    fun saveFavoriteId(context: Context?, cityId: Int, prefName: String = PREFERENCES_NAME) {
        if(context == null) return
        val sharedPreferences: SharedPreferences =
            context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putInt(FAVORITE_ID, cityId)
        editor.apply()
    }

    fun getFavoriteId(context: Context?, prefName: String = PREFERENCES_NAME): Int {
        if(context == null) return -1
        val sharedPreferences =
            context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        return sharedPreferences.getInt(FAVORITE_ID, -1)
    }
}