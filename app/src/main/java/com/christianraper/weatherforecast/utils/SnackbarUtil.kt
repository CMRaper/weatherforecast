package com.christianraper.weatherforecast.utils

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.christianraper.weatherforecast.R
import com.google.android.material.snackbar.Snackbar


object SnackbarUtil {
    fun failedSnackBar(view: View, msg: String, duration: Int){
        val snackbar = Snackbar.make(view, msg, duration)

        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(ContextCompat.getColor(view.context, R.color.colorSnackbarFailed))
        val textView =
            snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        snackbar.show()
    }

    fun successSnackBar(view: View, msg: String, duration: Int){
        val snackbar = Snackbar.make(view, msg, duration)

        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(ContextCompat.getColor(view.context, R.color.colorSnackbarSuccess))
        val textView =
            snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        snackbar.show()
    }
}