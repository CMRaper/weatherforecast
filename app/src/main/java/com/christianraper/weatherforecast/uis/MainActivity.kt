package com.christianraper.weatherforecast.uis

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.christianraper.weatherforecast.R
import com.christianraper.weatherforecast.databinding.ActivityMainBinding
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.others.ConstantManager
import com.christianraper.weatherforecast.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity(),
        ListFragment.ListFragmentListener{
    private val TAG_LIST = "tagList"
    private val TAG_CITY = "tagCity"

    private lateinit var binding: ActivityMainBinding
    private var listFragment: ListFragment? = null
    private var cityFragment: CityFragment? = null

    private var cityFragmentAttached = false
    private var listFragmentAttached = false

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpBinding()
        setUpToolbar()

        if(savedInstanceState != null){
            listFragmentAttached = savedInstanceState.getBoolean(ConstantManager.LIST_FRAGMENT_ATTACHED)
            cityFragmentAttached = savedInstanceState.getBoolean(ConstantManager.CITY_FRAGMENT_ATTACHED)
        }

        if(!listFragmentAttached){
            attachListFragment()
        }else{
            passTheAttachedInstanceOfListFragmentIfNotNull()
        }

        if(cityFragmentAttached){
            passTheAttachedInstanceOfCityFragmentIfNotNull()
            setBackIconVisibility(true)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        goingBackToListFragment()
    }

    override fun onSupportNavigateUp(): Boolean {
        goingBackToListFragment()
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(ConstantManager.LIST_FRAGMENT_ATTACHED, listFragmentAttached)
        outState.putBoolean(ConstantManager.CITY_FRAGMENT_ATTACHED, cityFragmentAttached)
        super.onSaveInstanceState(outState)
    }

    override fun onItemCityWeatherTapped(cityWeather: CityWeather) {
        if(!isListCurrentlyVisible()) return
        attachCityFragment(cityWeather)
        setBackIconVisibility(true)
    }

    private fun setUpBinding(){
        binding =  DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.vm = viewModel
        binding.lifecycleOwner = this
    }

    private fun setUpToolbar(){
        binding.toolbar.setTitle(R.string.app_name)
        setSupportActionBar(binding.toolbar)
    }

    private fun setBackIconVisibility(display: Boolean){
        supportActionBar?.setDisplayHomeAsUpEnabled(display);
        supportActionBar?.setDisplayShowHomeEnabled(display);
    }

    private fun getFragmentTransaction(): FragmentTransaction{
        val fragmentManager = supportFragmentManager
        return fragmentManager.beginTransaction()
    }

    private fun attachListFragment(){
        val transaction = getFragmentTransaction()
        listFragment = ListFragment.newInstance()
        transaction.add(R.id.fragment_holder_clayout, listFragment!!, TAG_LIST)
        transaction.commit()
        listFragmentAttached = true
    }

    private fun attachCityFragment(cityWeather: CityWeather){
        val transaction = getFragmentTransaction()
        cityFragment = CityFragment.newInstance(cityWeather.id)
        transaction.replace(R.id.fragment_holder_clayout, cityFragment!!, TAG_CITY)
        transaction.addToBackStack(null)
        transaction.commit()
        cityFragmentAttached = true
    }

    private fun goingBackToListFragment(){
        cityFragmentAttached = false
        passTheAttachedInstanceOfListFragmentIfNotNull()
        supportFragmentManager.popBackStack()
        setBackIconVisibility(false)
    }

    private fun isListCurrentlyVisible(): Boolean{
        val fragmentOnContainer: Fragment? =
            supportFragmentManager.findFragmentById(R.id.fragment_holder_clayout)
        return (fragmentOnContainer != null && fragmentOnContainer is ListFragment)
    }

    private fun passTheAttachedInstanceOfListFragmentIfNotNull(){
        val fragment: Fragment =
            supportFragmentManager.findFragmentById(R.id.fragment_holder_clayout)
                ?: return
            if(fragment is ListFragment){
                listFragment = fragment
            }
    }

    private fun passTheAttachedInstanceOfCityFragmentIfNotNull(){
        val fragment: Fragment =
            supportFragmentManager.findFragmentById(R.id.fragment_holder_clayout)
                ?: return
        if(fragment is CityFragment){
            cityFragment = fragment
        }
    }
}