package com.christianraper.weatherforecast.uis

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.christianraper.weatherforecast.R
import com.christianraper.weatherforecast.adapters.WeatherAdapter
import com.christianraper.weatherforecast.databinding.FragmentListBinding
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.others.ConstantManager
import com.christianraper.weatherforecast.others.ConstantManager.ERROR_MESSAGE_DURATION
import com.christianraper.weatherforecast.others.Status.*
import com.christianraper.weatherforecast.utils.SnackbarUtil
import com.christianraper.weatherforecast.viewmodels.ListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ListFragment: Fragment(), WeatherAdapter.CitiesWeatherListener {
    @Inject
    lateinit var weatherAdapter: WeatherAdapter

    private lateinit var listFragmentListener: ListFragmentListener
    private lateinit var binding: FragmentListBinding
    private val viewModel: ListViewModel by viewModels()
    private var loadedPreviously = false

    companion object {
        /**
         * @return A new instance of fragment ListFragment.
         */
        @JvmStatic
        fun newInstance() = ListFragment()
    }

    interface ListFragmentListener{
        fun onItemCityWeatherTapped(cityWeather: CityWeather)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ListFragmentListener) {
            listFragmentListener = context
        } else {
            throw RuntimeException("Implement the fragment interface")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setUpBinding(inflater, container)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadedPreviously = savedInstanceState?.getBoolean(ConstantManager.CITIES_WEATHER_LOADED) ?: false

        setUpRecyclerView()
        setUpWeatherObserver()
        fetchWeathers(loadedPreviously)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(ConstantManager.CITIES_WEATHER_LOADED, loadedPreviously)
    }

    override fun onItemCityWeatherTapped(cityWeather: CityWeather) {
        listFragmentListener.onItemCityWeatherTapped(cityWeather)
    }

    private fun setUpBinding(inflater: LayoutInflater, container: ViewGroup?){
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_list, container, false)
        binding.vm = viewModel
        binding.lifecycleOwner = this
    }

    private fun setUpWeatherObserver(){
        viewModel.weatherResource.observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    SUCCESS -> {
                        manageIsRefreshing()
                        resource.data?.let { response ->
                            manageTemplateText(
                                response.weathers.isEmpty(), getString(R.string.no_data_available)
                            )
                            setAdapterData(response.weathers)
                            loadedPreviously = true
                        }
                    }
                    ERROR -> {
                        SnackbarUtil.failedSnackBar(
                            binding.root,
                            getString(R.string.unable_to_fetch_data), ERROR_MESSAGE_DURATION)
                        manageTemplateText(
                            weatherAdapter.itemCount < 1, getString(R.string.unable_to_fetch_data)
                        )
                        manageIsRefreshing(false)
                    }
                    LOADING -> {
                        manageIsRefreshing(true)
                    }
                    SWIPE_REFRESH -> {
                        //do nothing
                    }
                }
            }
        })
    }

    private fun fetchWeathers(fetchFromLocal: Boolean){
        viewModel.fetchWeatherOfCities(viewModel.getCitiesToFetch(), fetchFromLocal)
    }

    private fun setUpRecyclerView(){
        val viewManager = LinearLayoutManager(context)
        weatherAdapter.setCityWeatherListener(this)
        binding.citiesWeatherRecyclerview.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = weatherAdapter
        }
    }

    private fun setAdapterData(weathers: List<CityWeather>) {
        weatherAdapter.apply {
            setCityWeathers(weathers)
            notifyDataSetChanged()
        }
    }

    private fun manageIsRefreshing(visible: Boolean = false){
        viewModel.isRefreshing.set(visible)
    }

    private fun manageTemplateText(visible: Boolean, label: String){
        viewModel.isNoDataVisible.set(visible)
        viewModel.templateText.set(label)
    }
}