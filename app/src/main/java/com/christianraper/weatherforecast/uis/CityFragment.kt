package com.christianraper.weatherforecast.uis

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.christianraper.weatherforecast.R
import com.christianraper.weatherforecast.databinding.FragmentCityBinding
import com.christianraper.weatherforecast.models.entities.CityWeather
import com.christianraper.weatherforecast.others.ConstantManager
import com.christianraper.weatherforecast.others.Status
import com.christianraper.weatherforecast.others.UIEvent
import com.christianraper.weatherforecast.utils.PreferencesManager
import com.christianraper.weatherforecast.utils.SnackbarUtil
import com.christianraper.weatherforecast.utils.Utilities
import com.christianraper.weatherforecast.viewmodels.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CityFragment : Fragment() {
    private lateinit var binding: FragmentCityBinding

    private val viewModel: DetailViewModel by viewModels()
    private var selectedCityId: Int? = null
    private var loadedPreviously = false
    companion object {
        @JvmStatic
        fun newInstance(selectedCityId: Int) =
            CityFragment().apply {
                arguments = Bundle().apply {
                    putInt(ConstantManager.SELECTED_CITY_ID, selectedCityId)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            selectedCityId = it.getInt(ConstantManager.SELECTED_CITY_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_city, container, false)
        binding.vm = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadedPreviously = savedInstanceState?.getBoolean(ConstantManager.CITY_WEATHER_LOADED) ?: false
        checkIfItemIsFavorite() //call this before observing isFavorite
        setUpWeatherObserver()
        setUpIsFavoriteObserver()
        fetchCityWeather(loadedPreviously)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(ConstantManager.CITY_WEATHER_LOADED, loadedPreviously)
    }

    private fun setUpWeatherObserver(){
        viewModel.weatherResource.observe(viewLifecycleOwner, {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { response ->
                            loadedPreviously = true
                            displayInformation(response)
                            manageCircularProgressVisibility(false)
                            manageFavoriteIconVisibility(true)
                        }
                    }
                    Status.ERROR -> {
                        SnackbarUtil.failedSnackBar(
                            binding.root, getString(R.string.unable_to_fetch_data),
                            ConstantManager.ERROR_MESSAGE_DURATION)
                        manageTemplateText(
                            true, getString(R.string.unable_to_fetch_data))
                        manageCircularProgressVisibility(false)
                    }

                    else -> manageCircularProgressVisibility(true)
                }
            }
        })
    }

    private fun setUpIsFavoriteObserver(){
        viewModel.uiEvent.observe(viewLifecycleOwner, {
            when(it){
                is UIEvent.FavoriteIconTapped -> {
                    PreferencesManager.saveFavoriteId(context, if(it.isFavorite) selectedCityId ?: -1 else -1)
                }
            }
        })
    }
    private fun fetchCityWeather(fetchFromLocal: Boolean){
        viewModel.fetchCityWeather(selectedCityId ?: 0, fetchFromLocal)
    }

    private fun manageTemplateText(visible: Boolean, label: String){
        viewModel.isNoDataVisible.set(visible)
        viewModel.templateText.set(label)
    }

    private fun manageCircularProgressVisibility(visible: Boolean = false){
        viewModel.isCircularProgressVisible.set(visible)
    }

    private fun manageFavoriteIconVisibility(visible: Boolean = false){
        viewModel.isFavoriteVisible.set(visible)
    }

    private fun displayInformation(cityWeather: CityWeather){
        val description = if(cityWeather.weather.isNotEmpty()) cityWeather.weather[0].main else ""

        binding.cityTextview.text = cityWeather.name
        binding.statusTextview.text = description
        binding.temperatureTextview.text =
            Utilities.formatTemperature(getString(R.string.temperature_placeholder),
                cityWeather.main?.temperature, 1)
        binding.highLowTextview.text =
            Utilities.formatHighLowTemperature(getString(R.string.high_low_placeholder),
            cityWeather.main?.tempMax, cityWeather.main?.tempMin)
    }

    private fun checkIfItemIsFavorite(){
        viewModel.showFilledIcon.set(Utilities.isFavorite(
            PreferencesManager.getFavoriteId(context), selectedCityId))
    }
}